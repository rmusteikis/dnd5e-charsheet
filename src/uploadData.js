function uploadData() {
  const firestoreService = require("firestore-export-import");
  const serviceAccount = require("../backend/serviceAccountKey.json");

  const databaseURL = "https://dnd5e-charsheet.firebaseio.com";

  firestoreService.initializeApp(serviceAccount, databaseURL);
  firestoreService.restore("../public/data.json");
  console.log("Uploaded");
}
export default uploadData;
