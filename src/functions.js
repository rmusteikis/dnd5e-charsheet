export function savedData() {
  const element = document.getElementById("saveBtn");
  element.innerHTML = "Saved";
  element.disabled = "true";
  setTimeout(() => {
    element.disabled = "";
    element.innerHTML = "Save";
  }, 3000);
}
