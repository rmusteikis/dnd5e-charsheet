import * as firebase from "firebase/app";
import app from "./firebase";

export async function getData() {
  const db = firebase.firestore(app);
  const usersRef = db
    .collection("users")
    .doc("6a7e5b30-e46f-11ea-841f-5fffa69161de");
  await usersRef.get().then((doc) => {
    if (doc.exists) {
      return doc.data().charactersData;
    } else {
      console.log(false);
    }
  });
}
