import React from "react";
import { TextArea } from "./TextArea";

export const AttackGroup = ({
  onChange,
  onTextFieldChange,
  textFields,
  state,
}) => {
  return (
    <div className="attackGroup">
      <div className="attack">
        <div className="title">Weapon</div>
        <div className="title">bonus</div>
        <div className="title">damage</div>
      </div>
      {state.attacks.map((a) => (
        <div className="attack" key={a.id} id={a.id}>
          <input type="text" value={a.weapon} onChange={onChange} id="weapon" />
          <input
            type="text"
            value={a.bonus}
            onChange={onChange}
            id="atkBonus"
          />
          <input
            type="text"
            value={a.damage}
            onChange={onChange}
            id="damageType"
          />
        </div>
      ))}
      <div id="attacks">
        <TextArea
          cols="30"
          rows="10"
          id="textarea"
          value={textFields[14].value}
          onChange={onTextFieldChange}
        ></TextArea>
      </div>
      <div className="title">Attacks & Spellcasting</div>
    </div>
  );
};
