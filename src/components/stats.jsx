import React, { useState } from "react";
import { Stat } from "./stat.jsx";
import { calcMod } from "./stat";

export function Stats() {
  const [str, setStr] = useState(16);
  const [dex, setDex] = useState(14);
  const [con, setCon] = useState(15);
  const [int, setInt] = useState(12);
  const [wis, setWis] = useState(10);
  const [cha, setCha] = useState(8);

  return (
    <div>
      <div className="stats">
        <Stat
          name="str"
          value={str}
          handleChange={(e) => setStr(e.target.value)}
        />
        <Stat
          name="dex"
          value={dex}
          handleChange={(e) => setDex(e.target.value)}
        />
        <Stat
          name="con"
          value={con}
          handleChange={(e) => setCon(e.target.value)}
        />
        <Stat
          name="int"
          value={int}
          handleChange={(e) => setInt(e.target.value)}
        />
        <Stat
          name="wis"
          value={wis}
          handleChange={(e) => setWis(e.target.value)}
        />
        <Stat
          name="cha"
          value={cha}
          handleChange={(e) => setCha(e.target.value)}
        />
      </div>
    </div>
  );
}
