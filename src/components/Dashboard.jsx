import React from "react";
import { withRouter } from "react-router";
import * as firebase from "firebase/app";
import app from "../firebase";
import { Navbar } from "./Navbar";
import { dummyData } from "../data";
import { Sheet } from "./dashboardSheet";

class Dashboard extends React.Component {
  state = {
    sheetsId: [],
    newSheetName: "",
    sheetData: {},
    spells: [],
    newSpell: "",
  };

  async componentDidMount() {
    const db = firebase.firestore(app);
    const usersRef = db
      .collection("users")
      .doc(firebase.auth().currentUser.uid)
      .collection("sheets")
      .get();

    await usersRef.then((snap) => {
      this.setState({ sheetCount: snap.size });
      if (snap.size > 0) {
        const sheetsId = snap.docs.map((doc) => doc.data().charName);
        this.setState({ sheetsId });
      }
    });
  }

  usersRef = () => {
    const db = firebase.firestore(app);
    const usersRef = db
      .collection("users")
      .doc(firebase.auth().currentUser.uid)
      .collection("sheets");
    return usersRef;
  };

  handleCreateSheet = async (id) => {
    const idLink = id.toLowerCase().replace(/\s+/g, "-");
    if (this.state.sheetsId.includes(idLink)) {
      this.setState({ newSheetName: "" });
    } else {
      const sheetsId = [...this.state.sheetsId, idLink];
      this.setState({ sheetsId });
      this.setState({ newSheetName: "" });
      await this.usersRef().doc(idLink).set(dummyData(idLink));
    }
  };

  handleSheetNameChange = (e) => {
    this.setState({ newSheetName: e.target.value });
  };

  handleSheetDelete = async (id) => {
    const sheetsId = [...this.state.sheetsId];
    const index = sheetsId.indexOf(id);
    if (index > -1) {
      sheetsId.splice(index, 1);
    }
    await this.usersRef()
      .doc(id)
      .delete()
      .then(console.log(id, "'s sheet deleted"));
    this.setState({ sheetsId });
  };

  handleSelectSheet = async (id) => {
    const state = await this.usersRef()
      .doc(id)
      .get()
      .then((doc) => doc.data());

    const location = {
      pathname: `/charsheet/${id}`,
      state: state,
    };
    this.props.history.push(location);
  };

  handleSpellChange = (e) => {
    const newSpell = e.target.value;
    this.setState({ newSpell });
  };

  handleAddSpell = () => {
    const spells = [this.state.newSpell, ...this.state.spells];
    this.setState({ spells });
    this.setState({ newSpell: "" });
  };

  render() {
    return (
      <div>
        <Navbar
          title={`Dashboard (${this.state.sheetsId.length})`}
          username={firebase.auth().currentUser.displayName}
          displayBtn={"none"}
        />
        <div className="sheetList">
          {this.state.sheetsId.length > 0 ? (
            this.state.sheetsId.map((id) => (
              <Sheet
                key={id}
                name={id}
                onSheetDelete={() => this.handleSheetDelete(id)}
                onSheetSelect={() => this.handleSelectSheet(id)}
              />
            ))
          ) : (
            <div className="noSheets"></div>
          )}
        </div>
        <div className="addSheet">
          <input
            className="form-control"
            type="text"
            placeholder="new sheet name"
            value={this.state.newSheetName}
            onChange={this.handleSheetNameChange}
          />
          <button
            className="btn btn-secondary"
            disabled={!this.state.newSheetName}
            onClick={() => this.handleCreateSheet(this.state.newSheetName)}
          >
            Add New Sheet
          </button>
        </div>
      </div>
    );
  }
}

export default withRouter(Dashboard);
