import React from "react";

export const calcMod = (inputValue) => {
  const mod = Math.floor((inputValue - 10) / 2);
  return mod;
};

export const Stat = ({ name, statValue, onChange }) => {
  return (
    <div className="stat">
      <span className="title">{name}</span>
      <input type="number" value={statValue} onChange={onChange} />
      <span className="statModifier">{calcMod(statValue || 0)}</span>
    </div>
  );
};
