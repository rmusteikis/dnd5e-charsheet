import React from "react";

export const Pagination = ({ items, onPageChange, currentPage }) => {
  const pages = ["Stats", "Attacks", "Bio"];
  return (
    <nav>
      <ul className="pagination">
        {pages.map((p) => (
          <li
            className={p === currentPage ? "page-item active" : "page-item"}
            key={p}
          >
            <button
              onClick={() => onPageChange(p)}
              className={
                p === currentPage
                  ? "btn btn-outline-secondary active"
                  : "btn btn-outline-secondary"
              }
            >
              {p}
            </button>
          </li>
        ))}
      </ul>
    </nav>
  );
};
