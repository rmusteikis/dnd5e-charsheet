import React from "react";
import { withRouter } from "react-router";
import { Navbar } from "./Navbar";

const CharSheet = (props) => {
  console.log(props.data);
  const { data } = props.location.state;
  return (
    <div>
      <Navbar title="Charsheet" />
      <p>{data ? data : "NO DATA"}</p>
    </div>
  );
};

export default withRouter(CharSheet);
