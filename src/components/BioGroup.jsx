import React from "react";
import { TextArea } from "./TextArea";

export const BioGroup = ({ state, onChange }) => {
  return (
    <div className="bioGroup greyBg">
      {state.slice(9, 13).map((field) => (
        <div className="bioItem" key={field.name} id={field.name}>
          <TextArea rows={field.rows} value={field.value} onChange={onChange} />
          <div className="title">{field.title}</div>
        </div>
      ))}
    </div>
  );
};
