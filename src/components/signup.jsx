import React, { useCallback } from "react";
import { withRouter } from "react-router";
import app from "../firebase";

export const SignUp = ({ history }) => {
  const handleSignUp = useCallback(
    async (event) => {
      event.preventDefault();
      const { email, password } = event.target.elements;
      try {
        await app
          .auth()
          .createUserWithEmailAndPassword(email.value, password.value);
        history.push("/");
      } catch (error) {
        alert(error);
      }
    },
    [history]
  );

  return (
    <div className="wrap">
      <div className="movingbg"></div>
      <div className="signup">
        <h1>Sign Up</h1>
        <form onSubmit={handleSignUp}>
          <input
            name="email"
            type="email"
            placeholder="email"
            className="form-control"
          />
          <input
            name="password"
            type="password"
            placeholder="password"
            className="form-control"
          />
          <button type="submit" className="btn btn-secondary">
            SignUp
          </button>
        </form>
        <span>Already have an account?</span>
        <a href="/login">Login!</a>
      </div>
    </div>
  );
};

export default withRouter(SignUp);
