import React from "react";

export const Spells = ({ spells, newSpell, onClick, onChange }) => {
  return (
    <div className="spells">
      <h1>Spells</h1>
      <p>implement search</p>
      <div className="addSpell">
        <input
          onChange={onChange}
          value={newSpell}
          name="newSpell"
          type="text"
          className="form-control"
        />
        <button
          disabled={!newSpell}
          onClick={onClick}
          className="btn btn-secondary"
        >
          Add a Spell
        </button>
      </div>
      {spells.map((spell) => (
        <p key={spell}>{spell}</p>
      ))}
    </div>
  );
};
