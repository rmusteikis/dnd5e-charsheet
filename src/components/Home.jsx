import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { withRouter } from "react-router";
import { Navbar } from "./Navbar";
import { UserContext } from "../UserContext";
import PrivateRoute from "../PrivateRoute";

function Home() {
  return (
    <div>
      <Navbar />
      <UserContext.Provider></UserContext.Provider>
    </div>
  );
}

export default withRouter(Home);
