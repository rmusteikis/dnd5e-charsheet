import React from "react";

export const TextField = ({ title, onChange, value, type }) => {
  return (
    <div className={"textField " + title} id={title}>
      <input type={type} value={value} onChange={onChange} />
      <div className="title">{title}</div>
    </div>
  );
};
