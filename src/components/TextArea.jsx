import React from "react";

export const TextArea = ({ className, rows, value, onChange }) => {
  return (
    <textarea
      className={className}
      cols="30"
      rows={rows}
      value={value}
      onChange={onChange}
    ></textarea>
  );
};
