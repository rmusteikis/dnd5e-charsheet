import React from "react";
import { TextField } from "./textField";

export const AcGroup = ({ textFields, onChange }) => {
  return (
    <div className="greyBg">
      <div className="acGroup">
        {textFields.slice(1, 4).map((f) => (
          <TextField
            key={f.name}
            title={f.name}
            type="number"
            value={f.value}
            onChange={onChange}
          />
        ))}
      </div>
      <div className="hpGroup">
        {textFields.slice(4, 7).map((f) => (
          <div className={f.name} id={f.name} key={f.name}>
            <div className="title">{f.title}</div>
            <input type="number" value={f.value} onChange={onChange} />
          </div>
        ))}
      </div>
      <div className="hitDiceGroup">
        <div className="hitDice">
          {textFields.slice(7, 9).map((f) => (
            <div id={f.name} key={f.name}>
              <div className="title">{f.title}</div>
              <input type="text" value={f.value} onChange={onChange} />
            </div>
          ))}
        </div>
        <div className="deathSaves">
          <div className="title m-b">Death Saves</div>
          <div className="saves">
            <div className="title">life</div>
            <div className="boxes" id="life">
              <input className="check" type="checkbox" />
              <input className="check" type="checkbox" />
              <input className="check" type="checkbox" />
            </div>
          </div>
          <div className="saves">
            <div className="title">death</div>
            <div className="boxes" id="death">
              <input className="check" type="checkbox" />
              <input className="check" type="checkbox" />
              <input className="check" type="checkbox" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
