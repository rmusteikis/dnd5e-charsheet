import React from "react";
import app from "../firebase";
import { NavLink } from "react-router-dom";

export function Navbar({ title, username, onSave, displayBtn }) {
  return (
    <div className="navbar">
      <h3 className="navbar-text">{title}</h3>
      <span className="navbar-text">{username}</span>
      <div className="navBtns">
        <button
          style={{ display: displayBtn }}
          id="saveBtn"
          className="btn-outline-danger btn  "
          onClick={onSave}
        >
          Save
        </button>
        <NavLink to="/">
          <button
            style={{ display: displayBtn }}
            className="btn btn-outline-secondary"
          >
            Sheets
          </button>
        </NavLink>
        <button
          className="btn btn-outline-secondary"
          onClick={() => app.auth().signOut()}
        >
          Log Out
        </button>
      </div>
    </div>
  );
}
