import React, { useCallback, useContext } from "react";
import { withRouter, Redirect } from "react-router";
import * as firebase from "firebase";
import app from "../firebase";
import { AuthContext } from "../Auth";

const Login = ({ history }) => {
  const handleGoogleLogin = useCallback(
    async (event) => {
      event.preventDefault();
      const provider = new firebase.auth.GoogleAuthProvider();
      try {
        await app
          .auth()
          .signInWithPopup(provider)
          .then((result) => {
            /* send to database */
            //const token = result.credential.accessToken;
            const user = result.user.toJSON();
            const db = firebase.firestore(app);
            const usersRef = db.collection("users");

            usersRef
              .doc(firebase.auth().currentUser.uid)
              .get()
              .then((doc) => {
                if (!doc.exists) {
                  usersRef
                    .doc(firebase.auth().currentUser.uid)
                    .set({ username: user.displayName });
                }
              });
          });
        history.push("/");
      } catch (error) {
        alert(error);
      }
    },
    [history]
  );

  var { currentUser } = useContext(AuthContext);

  if (currentUser) {
    return <Redirect to="/" />;
  }

  return (
    <div className="wrap">
      <div className="movingbg"></div>
      <div className="signup">
        <h1>EPIC!</h1>
        <p>
          DnD 5e online dynamic fillable <br />
          epic characters sheets
        </p>
        <form>
          <button
            className="btn btn-outline-danger"
            onClick={handleGoogleLogin}
          >
            Join with Google
          </button>
        </form>
      </div>
    </div>
  );
};
export default withRouter(Login);
