import React from "react";

export const Skill = (props) => {
  const {
    name,
    prop,
    mod,
    onProfChange,
    onExpChange,
    isSkills,
    isProf,
    isExp,
  } = props;

  return (
    <div className="skill">
      <div className="profCheckbox">
        {isSkills ? (
          <input
            className="skillExp"
            type="checkbox"
            checked={isExp}
            disabled={!isProf}
            onChange={onExpChange}
          />
        ) : (
          ""
        )}
        <input
          className="skillProf"
          type="checkbox"
          checked={isProf}
          disabled={isExp}
          onChange={onProfChange}
        />
      </div>
      <span className="skillMod"> {mod} </span>
      <span>
        {name} {isSkills ? <span style={{ color: "#999" }}>({prop})</span> : ""}
      </span>
    </div>
  );
};
