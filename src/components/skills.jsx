import React from "react";
import { Skill } from "./skill.jsx";

const skillList = [
  "Athletics",
  "Acrobatics",
  "Sleight of Hand",
  "Stealth",
  "Arcana",
  "History",
  "Investigation",
  "Nature",
  "Religion",
  "Animal Handling",
  "Insight",
  "Medicine",
  "Perception",
  "Survival",
  "Deception",
  "Intimidation",
  "Performance",
  "Persuasion",
];

export function Skills({ mod }) {
  return (
    <div className="skills">
      {skillList.map((skill) => (
        <Skill key={skill} name={skill} mod={mod} />
      ))}
    </div>
  );
}
