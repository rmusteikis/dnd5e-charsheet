import React from "react";
import app from "../firebase.js";
import * as firebase from "firebase/app";
import { Stat } from "./stat";
import { calcMod } from "./stat";
import { Skill } from "./skill";
import { Prof } from "./prof";
import { TextField } from "./textField";
import { TextArea } from "./TextArea";
import { AcGroup } from "./AcGroup";
import { AttackGroup } from "./AttackGroup";
import { EquipmentGroup } from "./EquipmentGroup";
import { BioGroup } from "./BioGroup";
import { Navbar } from "./Navbar";
import { SubNavbar } from "./subNavbar";
import { Pagination } from "./Pagination";
import { Loading } from "./Loading";
import { savedData } from "../functions";
import { withRouter } from "react-router";

function stateSet(state) {
  return {
    stats: state.stats,
    skills: state.skills,
    textFields: state.textFields,
    attackGroup: state.attackGroup,
    charAbstract: state.charAbstract,
    inspiration: state.inspiration,
    passive: state.passive,
    prof: state.prof,
    charName: state.charName,
  };
}

class CharactersSheet extends React.Component {
  state = {
    stats: [],
    skills: [],
    textFields: [],
    attackGroup: {},
    equipmentGroup: {},
    charAbstract: [],
    inspiration: 0,
    passive: 0,
    prof: 0,
    username: "",
    isLoading: true,
    currentPage: "Stats",
  };

  componentDidMount() {
    const state = this.props.location.state;
    this.setState(stateSet(state));
    this.setState({ isLoading: false });
  }

  handleSaveSheet = async () => {
    const db = firebase.firestore(app);
    const usersRef = db
      .collection("users")
      .doc(firebase.auth().currentUser.uid)
      .collection("sheets")
      .doc(this.state.charName);
    await usersRef
      .update(stateSet(this.state))
      .then(() => savedData())
      .catch((error) => console.error(error));
  };

  handleInspChange = (e) => {
    const inspiration = parseInt(e.target.value);
    this.setState({ inspiration });
  };

  handlePassiveChange = (e) => {
    const passive = parseInt(e.target.value);
    this.setState({ passive });
  };

  handleProfChange = (e) => {
    const maxProfValue = (value) => {
      //if (value === "") return 1;
      if (value >= 10) return 10;
      return value;
    };
    const prof = parseInt(maxProfValue(e.target.value));
    this.setState({ prof });
  };

  handleStatChange = (e, stat) => {
    const stats = [...this.state.stats];
    const index = stats.indexOf(stat);
    stats[index] = { ...stat };
    const maxStatValue = (value) => {
      //if (value === "") return 1;
      if (value > 30) return 30;
      return value;
    };
    stats[index].value = parseInt(maxStatValue(e.target.value));
    this.setState({ stats });
  };

  handleSkillProfBonus = (e, skill) => {
    const skills = [...this.state.skills];
    const index = skills.indexOf(skill);
    skills[index] = { ...skill };
    if (e.target.className === "skillProf") {
      skills[index].prof = !skills[index].prof;
    } else {
      skills[index].exp = !skills[index].exp;
    }
    this.setState({ skills });
  };

  handleSTProfBonus = (stat) => {
    const stats = [...this.state.stats];
    const index = stats.indexOf(stat);
    stats[index] = { ...stat };
    stats[index].prof = !stats[index].prof;
    this.setState({ stats });
  };

  calcSkillMods = (skill) => {
    return this.state.stats.map((stat) =>
      stat.name === skill.prop
        ? calcMod(stat.value) + this.profBonus(skill)
        : ""
    );
  };

  profBonus = (skill) => {
    const { prof } = this.state;
    return skill.exp ? 2 * prof : skill.prof ? prof : 0;
  };

  handleTextFieldChange = (e) => {
    const textFields = this.state.textFields;
    textFields.map((field) =>
      field.name === e.target.parentNode.id
        ? (field.value = e.target.value)
        : field.value
    );
    this.setState({ textFields });
  };

  handleAbstractChange = (e) => {
    const charAbstract = this.state.charAbstract;
    charAbstract.map((a) =>
      a.name === e.target.parentNode.id ? (a.value = e.target.value) : a.value
    );
    this.setState({ charAbstract });
  };

  handleAttackChange = (e) => {
    const attackGroup = { ...this.state.attackGroup };
    attackGroup.attacks.map((a) => {
      if (e.target.parentNode.id === a.id) {
        a.weapon = e.target.id === "weapon" ? e.target.value : a.weapon;
        a.bonus = e.target.id === "atkBonus" ? e.target.value : a.bonus;
        a.damage = e.target.id === "damageType" ? e.target.value : a.damage;
      }
      return a;
    });
    if (e.target.id === "textarea") {
      attackGroup.textField = e.target.value;
    }
    this.setState({ attackGroup });
  };

  handlePageChange = (page) => {
    this.setState({ currentPage: page });
  };

  handleSheets = () => {
    this.props.history.push("/");
  };

  render() {
    if (this.state.isLoading)
      return <Loading isLoading={this.state.isLoading} />;
    return (
      <div className="charSheet">
        <Navbar
          title="Characters Sheet"
          username={firebase.auth().currentUser.displayName}
          onSave={this.handleSaveSheet}
          onSheets={this.handleSheets}
        />
        <SubNavbar
          abstract={this.state.charAbstract}
          onChange={this.handleAbstractChange}
          onClick={this.handleAbstractClick}
        />
        <Pagination
          items={3}
          currentPage={this.state.currentPage}
          onPageChange={this.handlePageChange}
        />
        <div className="App">
          <div
            className="mainCol1"
            id="Stats"
            style={
              this.state.currentPage === "Stats" ? { display: "grid" } : {}
            }
          >
            <div className="stats">
              {this.state.stats.map((stat) => (
                <Stat
                  key={stat.name}
                  name={stat.name}
                  statValue={stat.value}
                  onChange={(e) => this.handleStatChange(e, stat)}
                />
              ))}
            </div>
            <TextField
              type={"number"}
              title={"inspiration"}
              value={this.state.inspiration}
              onChange={(e) => this.handleInspChange(e)}
            />
            <Prof
              profBonus={this.state.prof}
              onChange={(e) => this.handleProfChange(e)}
            />
            <div className="skillList statsGrid">
              {this.state.stats.map((s) => (
                <Skill
                  key={s.name}
                  name={s.name}
                  prop={s.prop}
                  isProf={s.prof}
                  isExp={s.exp}
                  mod={calcMod(s.value) + (s.prof ? this.state.prof : 0)}
                  onProfChange={() => this.handleSTProfBonus(s)}
                  isSkills={false}
                />
              ))}
              <div className="title">Saving Throws</div>
            </div>
            <div className="skillList skillsGrid">
              {this.state.skills.map((skill) => (
                <Skill
                  key={skill.name}
                  name={skill.name}
                  prop={skill.prop}
                  isProf={skill.prof}
                  isExp={skill.exp}
                  mod={this.calcSkillMods(skill)}
                  onProfChange={(e) => this.handleSkillProfBonus(e, skill)}
                  onExpChange={(e) => this.handleSkillProfBonus(e, skill)}
                  isSkills={true}
                />
              ))}
              <div className="title">Skills</div>
            </div>
            <TextField
              type={"text"}
              title={"passive"}
              value={this.state.passive}
              onChange={(e) => this.handlePassiveChange(e)}
            />
            <div className="col1-end" id="otherProfsAndLangs">
              <TextArea
                rows={14}
                value={this.state.textFields[0].value}
                onChange={this.handleTextFieldChange}
              />
              <div className="title">Other proficiencies & languages</div>
            </div>
          </div>
          <div
            className="mainCol2"
            id="Attacks"
            style={
              this.state.currentPage === "Attacks" ? { display: "grid" } : {}
            }
          >
            <AcGroup
              textFields={this.state.textFields}
              onChange={this.handleTextFieldChange}
            />
            <AttackGroup
              state={this.state.attackGroup}
              textFields={this.state.textFields}
              onChange={(e) => this.handleAttackChange(e)}
              onTextFieldChange={(e) => this.handleTextFieldChange(e)}
            />
            <EquipmentGroup
              state={this.state.equipmentGroup}
              onChange={this.handleEquipmentChange}
              textfield={this.state.textFields[15]}
              onTextFieldChange={(e) => this.handleTextFieldChange(e)}
            />
          </div>
          <div
            className="mainCol3"
            id="Bio"
            style={this.state.currentPage === "Bio" ? { display: "grid" } : {}}
          >
            <BioGroup
              state={this.state.textFields}
              onChange={(e) => this.handleTextFieldChange(e)}
            />
            <div className="bef" id="featuresAndTraits">
              <TextArea
                className="text"
                value={this.state.textFields[13].value}
                onChange={(e) => this.handleTextFieldChange(e)}
                rows={33}
              />
              <div className="title">Features & Traits</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(CharactersSheet);
