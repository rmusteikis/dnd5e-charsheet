import React from "react";

export const Sheet = ({
  name,
  onSheetDelete,
  onSheetSelect,
  onCogSelect,
  isCogSelected,
}) => {
  const cogBtnClass = isCogSelected
    ? "btn btn-secondary cogBtn"
    : "btn btn-secondary";
  return (
    <div className="sheet">
      <button onClick={onSheetSelect} className="btn sheet ">
        <h4>{name}</h4>
      </button>
      <button
        onClick={onSheetDelete}
        className="deleteSheet btn btn-outline-danger btn-sm"
      >
        <i className="fa fa-trash"></i>
      </button>
    </div>
  );
};
