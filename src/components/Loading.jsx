import React from "react";

export function Loading({ isLoading }) {
  if (isLoading)
    return (
      <div className="loading">
        <div></div>
      </div>
    );
  return "";
}
