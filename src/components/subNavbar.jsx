import React from "react";

export const SubNavbar = ({ abstract, onChange }) => {
  return (
    <div className="subNavbar">
      {abstract.map((a) => (
        <div key={a.name} className="subNavMenu" id={a.name}>
          <input
            type="text"
            placeholder={a.title}
            value={a.value}
            onChange={onChange}
          />
          <div className="title">{a.title}</div>
        </div>
      ))}
    </div>
  );
};
