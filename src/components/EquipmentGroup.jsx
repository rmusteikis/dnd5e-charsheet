import React from "react";
import { TextArea } from "./TextArea";

export const EquipmentGroup = ({
  state,
  onChange,
  onTextFieldChange,
  textfield,
}) => {
  return (
    <div className="equipmentGroup">
      <div className="money">
        <div className="coins">
          <p>CP</p>
          <input type="text" />
        </div>
        <div className="coins">
          <p>SP</p>
          <input type="text" />
        </div>
        <div className="coins">
          <p>EP</p>
          <input type="text" />
        </div>
        <div className="coins">
          <p>GP</p>
          <input type="text" />
        </div>
        <div className="coins">
          <p>PP</p>
          <input type="text" />
        </div>
      </div>
      <div id="equipment">
        <TextArea
          rows={17}
          className="textarea"
          value={textfield.value}
          onChange={onTextFieldChange}
        />
      </div>
      <div className="title">Equipment</div>
    </div>
  );
};
