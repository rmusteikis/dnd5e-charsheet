import React from "react";

export const Prof = ({ profBonus, onChange }) => {
  return (
    <div className="prof">
      <input
        className="profInput"
        type="number"
        value={profBonus}
        onChange={onChange}
      />
      <div className="title">Proficiency Bonus</div>
    </div>
  );
};
