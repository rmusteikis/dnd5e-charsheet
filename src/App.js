import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import CharactersSheet from "./components/CharactersSheet";
import Login from "./components/login";
import { AuthProvider } from "./Auth";
import PrivateRoute from "./PrivateRoute";
import Dashboard from "./components/Dashboard";

//implement hooks and pass them via context for dashboard and charsheet

function App() {
  return (
    <AuthProvider>
      <Router>
        <div>
          <PrivateRoute exact path="/" component={Dashboard} />
          <PrivateRoute
            exact
            path="/charsheet/:id"
            component={CharactersSheet}
          />
          <Route exact path="/login" component={Login} />
        </div>
      </Router>
    </AuthProvider>
  );
}

export default App;
