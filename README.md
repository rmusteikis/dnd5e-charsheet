# DEPRECATED!
<strong>The new project is located https://github.com/rmust/dnd5e-charsheet-pagan.</strong>

This was my first bigger ReactJS project. It works ok, but the code is messy and needs to be rewrited from the bottom.
Issues in this code: poor state management (no redux, etc.), poor code structure, messy and sometimes hardly unterstandable code, absolutely no tests, error logging, etc.

After testing in real Dungeons & Dragons games discovered following usability issuse:
* saving page and reloading it gives old data (poor context usage)
* styling is off in main sheet page
* cannot edit created sheet name
* sheet name is currently id-like, e.g. this-is-character instead This Is character
* delete sheet button is very dangerous (should add options button with drpdown)
* poor project management with git
* and some more.


# info
Fillable and automated character sheet of Dungeons and Dragons 5th edition made with ReactJS.

live project: https://dnd5e-charsheet.firebaseapp.com/

### FOR LEARNING PURPOSES ONLY!

# Deadline
Main works must be completed until the 1st of September, 2020.

# TODO
## Base
- [ ] Create Spell Sheet
- [ ] CLEAN UP THE CODE!
- [X] lift up the state!
- [X] ~~IMPLEMENT EVENT HANDLERS.~~
- [X] ~~Use Sass.~~
- [X] ~~Fix layout and styling of components.~~
- [X] Add stat bonuses depending on character race, class and background.
  - [X] Stat bonus from race.
  - [X] Stat bonus from class.
  - [X] Stat bonus from background.

## Components
### Co-dependant
- [X] ~~Stat -- for a single stat.~~
- [X] ~~Skill.~~

### Free (as in freedom) and fillable ??? 
- [X] ~~FillableField -- for every component that is fillable and does not connect with others.~~
